const random = num => {
  if (Number.isInteger(num)) {
    return Math.round(Math.random() * num);
  }
};

const fetchData = (url, data, method) => {
  // Default options are marked with *
  return fetch(url, {
    body: JSON.stringify(data), // must match 'Content-Type' header
    cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
    credentials: "same-origin", // include, same-origin, *omit
    headers: {
      "user-agent": "Mozilla/4.0 MDN Example",
      "content-type": "application/json"
    },
    method: method ? method : "GET", // *GET, POST, PUT, DELETE, etc.
    mode: "cors", // no-cors, cors, *same-origin
    redirect: "follow", // manual, *follow, error
    referrer: "no-referrer" // *client, no-referrer
  }).then(response => response.json()); // parses response to JSON
};

const globalErrorCallBack = (error, thisErrorCallBack) => {
  // 处理全局异常
  console.log(error);
  if (thisErrorCallBack) {
    thisErrorCallBack(error);
  }
};

export { random, fetchData, globalErrorCallBack };
