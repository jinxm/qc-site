const colors = [
  "#F4F4F6ff",
  "#E4E8EEff",
  "#D4DDE7ff",
  "#C3D1DFff",
  "#B3C5D7ff"
];

export { colors };
