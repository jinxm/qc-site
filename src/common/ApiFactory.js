import { fetchData, globalErrorCallBack } from "@/common/Utils";

const site = "http://localhost:8004";
const api = {
  // 登录
  login: (username, password, success, error) => {
    fetchData(
      `${site}/auth/login`,
      {
        username: username,
        password: password
      },
      "POST"
    )
      .then(success) // JSON from `response.json()` call
      .catch(errorData => globalErrorCallBack(errorData, error));
  },

  // 记录详情
  articleDetail: (id, success, error) => {
    fetchData(`${site}/article/${id}`, undefined, "GET")
      .then(success) // JSON from `response.json()` call
      .catch(errorData => globalErrorCallBack(errorData, error));
  },

  articleList: (success, error) => {
    fetchData(`${site}/article/list`, undefined, "GET")
      .then(success) // JSON from `response.json()` call
      .catch(errorData => globalErrorCallBack(errorData, error));
  }
};

export { api };
