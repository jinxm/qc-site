import Vue from "vue";
import VueRouter from "vue-router";

import Page1 from "@/views/Page1.vue";
import About from "@/views/About.vue";
import Index from "@/views/Index.vue";
import NotFound from "@/views/NotFound.vue";
import HomeBase from "@/views/home/HomeBase.vue";
import HomeIndex from "@/views/home/HomeIndex.vue";
import Me from "@/views/home/Me.vue";
// aMap bug 要指定路径
import Plan from "../views/home/Plan.vue";
import Show from "@/views/home/Show.vue";
import Article from "@/views/home/Article.vue";
import ArticleDetail from "@/views/home/ArticleDetail";

const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err);
};

Vue.use(VueRouter);
const routes = [
  {
    path: "/",
    name: "index",
    component: Index
  },
  {
    path: "/home/",
    name: "home",
    component: HomeBase,
    children: [
      {
        path: "",
        name: "home-index",
        component: HomeIndex
      },
      {
        path: "me",
        name: "home-me",
        component: Me
      },
      {
        path: "show",
        name: "home-show",
        component: Show
      },
      {
        path: "plan",
        name: "home-plan",
        component: Plan
      },
      {
        path: "article",
        name: "home-article",
        component: Article
      },
      {
        path: "article/:id",
        name: "home-article-detail",
        component: ArticleDetail
      }
    ]
  },
  {
    path: "/about/",
    name: "About",
    component: About
  },
  {
    path: "/page1/",
    name: "page1",
    component: Page1
  },
  {
    path: "/*",
    component: NotFound
  }
];

const router = new VueRouter({
  mode: "hash",
  base: process.env.BASE_URL,
  routes
});

export default router;
